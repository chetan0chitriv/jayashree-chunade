import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WenWomenComponent } from './wen-women.component';

describe('WenWomenComponent', () => {
  let component: WenWomenComponent;
  let fixture: ComponentFixture<WenWomenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WenWomenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WenWomenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
