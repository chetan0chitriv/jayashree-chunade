import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Summer21Component } from './summer21.component';

describe('Summer21Component', () => {
  let component: Summer21Component;
  let fixture: ComponentFixture<Summer21Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Summer21Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Summer21Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
