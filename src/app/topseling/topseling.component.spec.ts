import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopselingComponent } from './topseling.component';

describe('TopselingComponent', () => {
  let component: TopselingComponent;
  let fixture: ComponentFixture<TopselingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopselingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopselingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
