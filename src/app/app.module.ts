import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {MatGridListModule} from '@angular/material/grid-list';
import { MatCheckboxModule } from '@angular/material/checkbox';

 
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { IndexComponent } from './index/index.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSliderModule } from '@angular/material/slider';

import { SaleComponent } from './sale/sale.component';
import { Summer21Component } from './summer21/summer21.component';
import { ContactComponent } from './contact/contact.component';
import { ReturnComponent } from './return/return.component';
import { ShippingComponent } from './shipping/shipping.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TrackComponent } from './track/track.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FetureprdComponent } from './fetureprd/fetureprd.component';
import { SubscribeComponent } from './subscribe/subscribe.component';
import { SliderComponent } from './slider/slider.component';
import { WenWomenComponent } from './wen-women/wen-women.component';
import { TopselingComponent } from './topseling/topseling.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ImagesComponent } from './images/images.component';
import { ProductsComponent } from './products/products.component';
import { BrandComponent } from './brand/brand.component';
import { Brand1Component } from './brand1/brand1.component';
import { ForgetpassComponent } from './forgetpass/forgetpass.component';


@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    HeaderComponent,
    FooterComponent,
    SaleComponent,
    Summer21Component,
    ContactComponent,
    ReturnComponent,
    ShippingComponent,
    TermsComponent,
    PrivacyComponent,
    TrackComponent,
    LoginComponent,
    RegisterComponent,
    FetureprdComponent,
    SubscribeComponent,
    SliderComponent,
    WenWomenComponent,
    TopselingComponent,
    ImagesComponent,
    ProductsComponent,
    BrandComponent,
    Brand1Component,
    ForgetpassComponent,
    LoginComponent
    
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatSidenavModule,
    MatMenuModule,
    MatGridListModule,
    MatCardModule,
    FlexLayoutModule,
    MatFormFieldModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatCheckboxModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
