import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-forgetpass',
  templateUrl: './forgetpass.component.html',
  styleUrls: ['./forgetpass.component.css']
})
export class ForgetpassComponent implements OnInit {

  submitForm : FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.submitForm = new FormGroup({
      email : new FormControl('',[Validators.required,Validators.email]),
     }
  
     );
      }
    onsubmit()
    {}
  }

