import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrandComponent } from './brand/brand.component';
import { Brand1Component } from './brand1/brand1.component';
import { ContactComponent } from './contact/contact.component';
import { ForgetpassComponent } from './forgetpass/forgetpass.component';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { ProductsComponent } from './products/products.component';
import { RegisterComponent } from './register/register.component';
import { ReturnComponent } from './return/return.component';
import { ShippingComponent } from './shipping/shipping.component';
import { TermsComponent } from './terms/terms.component';
import { TrackComponent } from './track/track.component';

const routes: Routes = 
[
  { path: '', component :IndexComponent},
  { path: 'index', component:IndexComponent},
  { path: 'contact', component :ContactComponent },
  { path: 'return', component : ReturnComponent },
  { path: 'shipping', component : ShippingComponent },
  { path: 'terms', component : TermsComponent },
  { path: 'privacy', component : PrivacyComponent },
  { path: 'track', component : TrackComponent },
  { path: 'login', component : LoginComponent },
  { path: 'register', component : RegisterComponent },
  { path: 'brand', component : BrandComponent },
  { path: 'brand1', component : Brand1Component },
  { path: 'products', component : ProductsComponent },
  { path: 'forgetpass', component : ForgetpassComponent}

  
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
