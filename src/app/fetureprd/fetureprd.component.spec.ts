import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FetureprdComponent } from './fetureprd.component';

describe('FetureprdComponent', () => {
  let component: FetureprdComponent;
  let fixture: ComponentFixture<FetureprdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FetureprdComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FetureprdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
